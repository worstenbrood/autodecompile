IF "%1"=="" GOTO QUIT
ECHO [+] Decompiling %~n1%~x1 ...
MD "%~dp0temp_%~n1" 2>NUL 1>&2
"%~dp0%TOOLS_DIR%\7za.exe" x -y -o"%~dp0temp_%~n1" "%1" 2>NUL 1>&2
IF NOT "%ERRORLEVEL%"=="0" GOTO ERROR
IF NOT EXIST "%~n1.odex" (
	java -Xmx512m -jar "%~dp0%TOOLS_DIR%\%BAKSMALI%" -a %SDK_VERSION%%IGNORE% -d "%~dp0%FRAMEWORK_DIR%" -c "%BOOTCLASSPATH%" -o "%~dp0out_%~n1" "%~dp0temp_%~n1\classes.dex"
) ELSE (
	java -Xmx512m -jar "%~dp0%TOOLS_DIR%\%BAKSMALI%" -a %SDK_VERSION%%IGNORE% -d "%~dp0%FRAMEWORK_DIR%" -c "%BOOTCLASSPATH%" -x %~n1.odex-o "%~dp0out_%~n1"
)
IF "%ERRORLEVEL%"=="1" GOTO ERROR
GOTO NOERROR
:ERROR
ECHO [%TIME:~0,2%:%TIME:~3,2% %DATE:~3,2%/%DATE:~6,2%/%DATE:~9,4%] Deodex Failed: %1 >> "%~dp0error.log"
ECHO [-] Error while deodexing %~n1%~x1 !
RD /s /q "%~dp0temp_%~n1" 2>NUL 1>&2
RD /s /q "%~dp0out_%~n1" 2>NUL 1>&2
GOTO QUIT
:NOERROR
ECHO [+] Succesfully decompiled %~n1%~x1 !
:QUIT
