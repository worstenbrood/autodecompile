@ECHO OFF

SET FRAMEWORK_DIR=framework
SET TOOLS_DIR=tools
SET SMALI=smali-1.3.3.jar
SET BAKSMALI=baksmali-1.3.3.jar
SET BOOTCLASSPATH=:core.jar:ext.jar:framework.jar:android.policy.jar:services.jar
SET COMPRESSIONLEVEL=9
SET DECOMPILE=0
SET IGNORE=
SET COMPILE=0
SET SDK_VERSION=13
SET INPUT_FILE=

ECHO [+] AutoDecompile v1.0 (worstenbrood@gmail.com)
ECHO -----------------------------------------------
ECHO.

SET INPUT_FILE=%1
SHIFT /1

:LOOP
IF "%1"=="" GOTO CONTINUE
IF "%1"=="/i" (
	SET IGNORE= -I
	GOTO NEXT
)
IF "%1"=="/d" (
	SET DECOMPILE=1
	GOTO NEXT
)
IF "%1"=="/c" (
	SET COMPILE=1
	GOTO NEXT
)
IF "%1"=="/b" (
	IF NOT "%2"=="" (
		SET BOOTCLASSPATH=%BOOTCLASSPATH%:%2
		SHIFT /1
	)
	GOTO NEXT
)
IF "%1"=="/x" (
	IF NOT "%2"=="" (
		SET COMPRESSIONLEVEL=%2
		SHIFT /1
	)
	GOTO NEXT
)
IF "%1"=="/v" (
	IF NOT "%2"=="" (
		SET SDK_VERSION=%2
		SHIFT /1
	)
	GOTO NEXT
)
IF "%1"=="/?" (
	ECHO %~n0%~x0 file.apk/jar [/d] [/c] [/i] [/b class] [/x compressionlevel] [/v SDK_VERSION]
	ECHO.
	ECHO /d: Decompile file
	ECHO /c: Compile
	ECHO /i: Ignore baksmali errors
	ECHO /b class: Add class to bootclasspath (Default: %BOOTCLASSPATH%)
	ECHO /x compressionlevel: Set APK/JAR compression level (Default: %COMPRESSIONLEVEL%)
	ECHO /v SDK_VERSION: Set SDK version of rom  (Default: %SDK_VERSION%)
	GOTO QUIT
)
:NEXT
SHIFT /1
GOTO LOOP
:CONTINUE

echo "File: %INPUT_FILE%"

ECHO [+] Checking java ...
java -version 2>NUL 1>&2
IF NOT "%ERRORLEVEL%"=="0" (
	ECHO [-] Java not found !
	GOTO QUIT
)

ECHO [+] Building bootclass path ...
FOR %%F IN (%~dp0%FRAMEWORK_DIR%\*.jar) DO CALL buildbootclasspath.bat %%F
IF "%BOOTCLASSPATH%"=="" (
	ECHO [-] BootClassPath is empty !
	GOTO QUIT
)
ECHO [+] BootClassPath: %BOOTCLASSPATH%

ECHO [+] Checking directories ...
FOR %%D IN ("%~dp0%FRAMEWORK_DIR%") DO (
	IF NOT EXIST "%%D\." (
		ECHO [-] Directory %%D not found !
		GOTO QUIT
	)
)

IF "%DECOMPILE%"=="1" (
	ECHO [+] Decompiling %INPUT_FILE% ...
	CALL decompile.bat %INPUT_FILE%
)

IF "%COMPILE%"=="1" (
	ECHO [+] Compiling %INPUT_FILE% ...
	CALL compile.bat %INPUT_FILE%
)

:QUIT
SET DEODEX_PRE=
SET APP_DIR=
SET FRAMEWORK_DIR=
SET TOOLS_DIR=
SET SMALI=
SET BAKSMALI=
SET BOOTCLASSPATH=
SET COMPRESSIONLEVEL=
SET DECOMPILE=
SET COMPILE=
SET IGNORE=
SET SDK_VERSION=