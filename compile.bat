IF "%1"=="" GOTO QUIT
ECHO [+] Compiling %~n1%~x1 ...
java -jar "%~dp0%TOOLS_DIR%\%SMALI%" -a 13 "%~dp0out_%~n1" -o "%~dp0temp_%~n1\classes.dex"
IF "%ERRORLEVEL%"=="1" GOTO ERROR
"%~dp0%TOOLS_DIR%\7za.exe" a -y -tzip "%~n1-new%~x1" "%~dp0temp_%~n1\*" -mx%COMPRESSIONLEVEL% 2>NUL 1>&2
IF NOT "%ERRORLEVEL%"=="0" GOTO ERROR
GOTO NOERROR
:ERROR
ECHO [%TIME:~0,2%:%TIME:~3,2% %DATE:~3,2%/%DATE:~6,2%/%DATE:~9,4%] Deodex Failed: %1 >> "%~dp0error.log"
ECHO [-] Error while deodexing %~n1%~x1 !
GOTO QUIT
:NOERROR
ECHO [+] Succesfully compiled %~n1%~x1 !
RD /s /q "%~dp0temp_%~n1" 2>NUL 1>&2
RD /s /q "%~dp0out_%~n1" 2>NUL 1>&2
:QUIT
